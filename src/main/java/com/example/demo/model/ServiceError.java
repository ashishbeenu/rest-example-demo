package com.example.demo.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ServiceError implements Serializable{

	private static final long serialVersionUID = -2790536096116092073L;

	@JsonProperty("erroCode")
	private String erroCode;

	@JsonProperty("erroDesc")
	private String erroDesc;

	@JsonProperty("erroCode")
	public String getErroCode() {
		return erroCode;
	}

	@JsonProperty("erroCode")
	public void setErroCode(String erroCode) {
		this.erroCode = erroCode;
	}

	@JsonProperty("erroDesc")
	public String getErroDesc() {
		return erroDesc;
	}

	@JsonProperty("erroDesc")
	public void setErroDesc(String erroDesc) {
		this.erroDesc = erroDesc;
	}


}
