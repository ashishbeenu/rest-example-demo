package com.example.demo.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.example.demo.exception.ServiceException;
import com.example.demo.model.Customer;


@Component
public class CustomerDAO {
	private static List<Customer>  customerList = new ArrayList<Customer>();
	{
		Customer c1 = new Customer();
		c1.setId(1);
		c1.setAge(10);
		c1.setName("Ashish");
		c1.setAddress("Bangalore");
		customerList.add(c1);

		Customer c2 = new Customer();
		c2.setId(2);
		c2.setAge(20);
		c2.setName("Scot");
		c2.setAddress("USA");
		customerList.add(c2);

		Customer c3 = new Customer();
		c3.setId(3);
		c3.setAge(30);
		c3.setName("SAM");
		c3.setAddress("CA");
		customerList.add(c3);
	}
	public Customer getCustomerById(Integer id) {
		Customer customer = null;
		if(customerList !=null && !customerList.isEmpty()){
			for (Customer cust : customerList) {
				if(cust.getId() ==id){
					customer = cust;
					break;
				}
			}
		} 
		return customer;
	}

	public Customer addCustomer(Customer customer) throws ServiceException{
		//Check customer must not be exist in the list
		Customer cust = getCustomerById(customer.getId());
		if(cust != null && (cust.getId() == customer.getId())){
			throw new ServiceException("1010", "Customer is already exist ", HttpStatus.CONFLICT);
		}
		
		customerList.add(customer);
		return customer;
	}

	public Customer deleteCustomerById(Integer id) {
		Iterator<Customer> iterator = customerList.iterator();
		while(iterator.hasNext()) {
			Customer next = iterator.next();
			if(next.getId() == id){
				customerList.remove(next);
				return next;
			}
		}	
		return null;
	}

	public List<Customer> searchCustomer(String name) {
		List<Customer> list = new ArrayList<Customer>();
		if(customerList !=null && !customerList.isEmpty()){
			for (Customer cust : customerList) {
				if(cust.getName().equalsIgnoreCase(name)){
					list.add(cust);
				}
			}
		} 
		return list;
	}
}
