package com.example.demo.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exception.ServiceException;
import com.example.demo.model.Customer;

@RestController

//@RequestMapping("/users")
public interface CustomerController {

	@GetMapping(value = "/api", produces = "application/json")
	public String test();
	
	//http://localhost:9000/rest/customers/customer/1
	//OR
	//@RequestMapping(value="/customers/customer/{id}",method=RequestMethod.GET,consumes="application/json",produces="application/json")
	@GetMapping(value = "/customers/customer/{id}", produces = "application/json")
	public 	Customer getCustomer(@PathVariable (value="id") Integer id) throws ServiceException;

	@PostMapping(value = "/customers/customer", produces = "application/json")
	public 	Customer addCustomer(@RequestBody Customer customer)throws ServiceException;

	//http://localhost:9000/rest/customers/customer/1
	@DeleteMapping(value = "/customers/customer/{id}", produces = "application/json")
	public 	Customer deleteCustomer(@PathVariable (value="id") Integer id)throws ServiceException;

	//http://localhost:9000/rest/customers/?name=Ashish
	@GetMapping(value = "/customers", produces = "application/json")
	public 	List<Customer> searchCustomer(@RequestParam String name)throws ServiceException;

	//http://localhost:9000/rest/customers/responseEntity/?name=Ashish
	@GetMapping(value = "/customers/responseEntity", produces = "application/json")
	public 	ResponseEntity<List<Customer>> myCustomer(@RequestParam String name) throws ServiceException;
}
