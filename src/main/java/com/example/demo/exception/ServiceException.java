package com.example.demo.exception;

import org.springframework.http.HttpStatus;

public class ServiceException extends Exception{

	private static final long serialVersionUID = 1L;
	
	private String erroCd;
	private String message;
	private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
	
	public String getErroCd() {
		return erroCd;
	}
	public void setErroCd(String erroCd) {
		this.erroCd = erroCd;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}
	public ServiceException(String erroCd,String message,HttpStatus httpStatus){
		super(message);
		this.erroCd=erroCd;
		this.message=message;
		this.httpStatus = httpStatus;
	}

}