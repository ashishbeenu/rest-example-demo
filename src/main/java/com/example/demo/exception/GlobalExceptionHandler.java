package com.example.demo.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.example.demo.model.ServiceError;
//https://spring.io/blog/2013/11/01/exception-handling-in-spring-mvc

@ControllerAdvice
public class GlobalExceptionHandler {
	@ExceptionHandler(ServiceException.class)
	public ResponseEntity<ServiceError> exceptionHandler(ServiceException ex){
		ServiceError serviceError = new ServiceError();
		serviceError.setErroCode(ex.getErroCd());
		serviceError.setErroDesc(ex.getMessage());
		return new ResponseEntity<ServiceError>(serviceError,ex.getHttpStatus());
	}

}
