package com.example.demo.validator;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.example.demo.exception.ServiceException;
import com.example.demo.model.Customer;
@Component
public class Validator {

	/**
	 * To validate name 
	 * @param name String
	 * @throws ServiceException if given string name is null , empty or  more than 30 character
	 */
	public void validateName(String name)throws ServiceException{
		if(name == null || name.trim().length() ==0){
			throw new ServiceException("1001", "name can not be empty", HttpStatus.BAD_REQUEST);
		}
		if(name.trim().length() > 20){
			throw new ServiceException("1002", "name length cannot be more than 30 character", HttpStatus.BAD_REQUEST);
		}
	}
	/**
	 * To validate customer details
	 * @param customer
	 * @throws ServiceException
	 */
	public void validateCustomerDetails(Customer customer)throws ServiceException{
		if(customer == null){
			throw new ServiceException("1003", "Body can not be empty", HttpStatus.BAD_REQUEST);
		}
		if(customer.getId() == null){
			throw new ServiceException("1004", "customer id missing", HttpStatus.BAD_REQUEST);
		}
		if(customer.getId() == 0){
			throw new ServiceException("1005", "customer id can not be 0", HttpStatus.BAD_REQUEST);
		}

		if(customer.getAge() == null){
			throw new ServiceException("1006", "customer age missing", HttpStatus.BAD_REQUEST);
		}
		if(customer.getAge() == 0){
			throw new ServiceException("1007", "customer age can not be 0", HttpStatus.BAD_REQUEST);
		}

		validateName(customer.getName());

		if(customer.getAddress() == null){
			throw new ServiceException("1008", "Customer address missing", HttpStatus.BAD_REQUEST);
		}
		if(customer.getAddress().trim().length() > 70){
			throw new ServiceException("1009", "address length cannot be more than 70 character", HttpStatus.BAD_REQUEST);
		}
	}
}
