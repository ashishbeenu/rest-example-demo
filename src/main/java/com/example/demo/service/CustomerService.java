package com.example.demo.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.example.demo.controller.CustomerController;
import com.example.demo.dao.CustomerDAO;
import com.example.demo.exception.ServiceException;
import com.example.demo.model.Customer;
import com.example.demo.model.ServiceError;
import com.example.demo.validator.Validator;
/**
 * 
 * @author Ashish
 *
 */
@Service
public class CustomerService implements CustomerController{

	@Autowired
	private CustomerDAO dao;
	
	@Autowired
	private Validator validator;
	
	private static final Logger log = LoggerFactory.getLogger(CustomerService.class);
	
	@Override
	public String test() {
		log.info("Service invoked success...");
		return "Service working ....";
	}

	
	@Override
	public Customer getCustomer(Integer id) throws ServiceException{
		Customer customer = dao.getCustomerById(id);
		if(customer == null){
			throw new ServiceException("1003", "Customer not found", HttpStatus.NOT_IMPLEMENTED);
		}
		return dao.getCustomerById(id);
	}
	
	@Override
	public Customer addCustomer(Customer customer) throws ServiceException{
		validator.validateCustomerDetails(customer);
		return dao.addCustomer(customer);
	}

	@Override
	public Customer deleteCustomer(Integer id) throws ServiceException{
		Customer customer = dao.deleteCustomerById(id);
		if(customer == null){
			throw new ServiceException("1003", "Customer not found", HttpStatus.NOT_IMPLEMENTED);
		}
		return customer;
	}

	@Override
	public List<Customer> searchCustomer(String name) throws ServiceException{
		validator.validateName(name);
		return dao.searchCustomer(name);
	}

	@Override
	public ResponseEntity<List<Customer>> myCustomer(String name) throws ServiceException {
		validator.validateName(name);
		List<Customer> customrer = dao.searchCustomer(name);
		return new ResponseEntity<List<Customer>>(customrer,HttpStatus.OK);
	}
}
